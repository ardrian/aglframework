AGLFramework
------------
A simple test framework for tiled OpenGL images. At this point this is an exercise providing a framework for one wanting to use OpenGL directly for rendering images downloaded via a URL. 

99% of the time this would not be the right way to go. For starters a UICollectionView with 3D objects could be used. Secondly, it may be possible to use Core Animation instead. In both these cases it would be easier to implement animations and transitions, using existing iOS frameworks. 

For cases where we may want thousands of objects, along with lighting, it may be interesting to employ a direct OpenGL approach.

### Usage
Subclass the AGLViewController class. This subclass, or a separate class should implement the methods in the AGLQuadSetDelegate protocol.

Notes
--------
This isn't an efficient implementation. 

### Draw Calls 
It currently makes an OpenGL draw call for each object in the scene. If a subset of objects shared the same texture, or we automatically added downloaded thumbnails to a texture-atlas on the fly, we could reduce draw-calls by batching the quad-drawing.

### Picking
Picking currently involves two matrix inversions for each object. This is going to be slow, and could easily be optimised with some cheaper initial bounds tests. 



