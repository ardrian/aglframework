//
//  AGLViewController.m
//  AGLExample
//
//  Created by Ardrian Hardjono on 23/12/2013.
//  Copyright (c) 2013 Claire's. All rights reserved.
//

#import "AGLExampleViewController.h"

@interface AGLExampleViewController ()<AGLQuadSetDelegate>

@end

@implementation AGLExampleViewController

- (id)init{
    self = [super init];
    if(self){
        self.quadSetDelegate = self;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blueColor];
    //self.quadSetDelegate = self;
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - AGLQuadSetDelegate methods
- (NSInteger)numItemsInQuadSet{
    return 2;
}

- (NSURL*)urlForItemInQuadSetWithIndex:(NSUInteger)index{
    if(index == 0){
        NSURL* url = [NSURL URLWithString:@"http://placehold.it/100x100"];
        return url;
    } else if (index == 1){
        NSURL* url = [NSURL URLWithString:@"http://placehold.it/100x100"];
        return url;
    }
    return nil;
}

- (void)quadPressed:(NSUInteger)index{
    NSLog(@"Item Pressed: %u",index);
}

@end
