//
//  AGLAppDelegate.h
//  AGLExample
//
//  Created by Ardrian Hardjono on 23/12/2013.
//  Copyright (c) 2013 Claire's. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIViewController *viewController;
@end
