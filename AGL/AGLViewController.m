//
//  AGLViewController.m
//  AGLFramework
//
//  Created by Ardrian Hardjono on 23/12/2013.
//
//

#import "AGLViewController.h"
#import "AGLTextureQuadManager.h"

@interface AGLViewController (){
    float _rotation;
    GLKMatrix4 _viewMatrix;
    GLKMatrix4 _projectionMatrix;
}

@property (strong, nonatomic) EAGLContext *context;
@property (strong, nonatomic) AGLTextureQuadManager* texturedQuadsManager;

@end

@implementation AGLViewController

#pragma mark Allocation/Deallocation
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark - View lifecycle

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
 }
 */

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    view.drawableMultisample = GLKViewDrawableMultisample4X;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    
    self.texturedQuadsManager = [[AGLTextureQuadManager alloc] init];
    self.texturedQuadsManager.delegate = self.quadSetDelegate;
    self.texturedQuadsManager.context = self.context;
    
    [self setupGL];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
    self.context = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Open GL Management
- (void)setupGL {
    
    [EAGLContext setCurrentContext:self.context];
    self.texturedQuadsManager.context = self.context;
    [self.texturedQuadsManager setupGL];
    [self.texturedQuadsManager updateLayout];
    
}

- (void)tearDownGL {
    
    [EAGLContext setCurrentContext:self.context];
    [self.texturedQuadsManager tearDownGL];
    
}

#pragma mark - GLKViewDelegate
- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect {
    
    glClearColor(0.5, 0.0, 0.5, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    
    [self.texturedQuadsManager drawWithViewMatrix:_viewMatrix];
}

#pragma mark - GLKViewControllerDelegate
- (void)update{
    // Projection Matrix Setup
    float aspect = fabsf(self.view.bounds.size.width / self.view.bounds.size.height);
    _projectionMatrix = GLKMatrix4MakePerspective(GLKMathDegreesToRadians(65.0f), aspect, 4.0f, 10.0f);
    self.texturedQuadsManager.effect.transform.projectionMatrix = _projectionMatrix;
    
    // Modelview Matrix Setup
    _viewMatrix =GLKMatrix4MakeTranslation(0.0f, 0.0f, -6.0f);
}

#pragma mark - GLKViewControllerDelegate

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch* touch = [[event touchesForView:self.view] anyObject];
    
    CGPoint location = [touch locationInView:self.view];
    
    // Invert location for the GL coordinate system 
    location.y = self.view.bounds.size.height - location.y;

    int viewport[4];
    viewport[0] = 0.0f;
    viewport[1] = 0.0f;
    viewport[2] = self.view.bounds.size.width;
    viewport[3] = self.view.bounds.size.height;
    
    NSUInteger hitIndex = 0;
    if([self.texturedQuadsManager didHitObject:location
                             withViewMatrix:_viewMatrix
                           projectionMatrix:_projectionMatrix
                                   viewport:viewport
                                      hitIndex:&hitIndex]){
        [self.quadSetDelegate quadPressed:hitIndex];
    }

}

@end
