//
//  AGLQuadSetDelegate.h
//  AGLFramework
//
//  Created by Ardrian Hardjono on 21/12/2013.
//
//

#import <Foundation/Foundation.h>

@protocol AGLQuadSetDelegate <NSObject>

- (NSInteger)numItemsInQuadSet;
- (NSURL*)urlForItemInQuadSetWithIndex:(NSUInteger)index;
- (void)quadPressed:(NSUInteger)index;

@end
