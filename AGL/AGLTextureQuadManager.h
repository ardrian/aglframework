//
//  AGLTextureQuadManager.h
//  AGLFramework
//
//  Created by Ardrian Hardjono on 21/12/2013.
//
// Manages the rendering of a set of texture quads. This class will
// only be referenced by internal AGL classes

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>
#import "AGLQuadSetDelegate.h"
#import "AGLLayout.h"

@interface AGLTextureQuadManager : NSObject

@property (weak, nonatomic) id<AGLQuadSetDelegate> delegate;
@property (strong, nonatomic) EAGLContext *context;
@property (strong, nonatomic) GLKBaseEffect *effect;

/** Initialisation */
- (id)init; 
- (id)initWithLayout:(AGLLayout*)layout;

/** OpenGL */
- (void)setupGL;
- (void)tearDownGL;

/** Called each frame */
- (void)drawWithViewMatrix:(GLKMatrix4)viewMatrix;
- (void)update;

/** Called to trigger layout updates */
- (void)updateLayout; 

/** Intersection Testing */
- (BOOL)didHitObject:(CGPoint)location
      withViewMatrix:(GLKMatrix4)viewMatrix
    projectionMatrix:(GLKMatrix4)projectionMatrix
            viewport:(int*)viewport
            hitIndex:(NSUInteger*)hitIndex;

@end
