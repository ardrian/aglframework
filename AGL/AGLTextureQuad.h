//
//  AGLTextureQuad.h
//  AGLFramework
//
//  Created by Ardrian Hardjono on 21/12/2013.
//
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>

@interface AGLTextureQuad : NSObject
- (void)setupGL;
- (void)setPNGWithName:(NSString*)name withShareGroup:(EAGLSharegroup*)shareGroup;
- (void)setTextureWithURL:(NSURL*)url withShareGroup:(EAGLSharegroup*)shareGroup;
- (void)draw;
- (void)tearDownGL;
- (GLKMatrix4)transformationMatrix;
- (BOOL)isPointOnPlane:(GLKVector3)point;
- (void)showHit:(BOOL)hit;

@property (strong,nonatomic) NSString* name; 
@property CGFloat width;
@property CGFloat height;
@property GLKVector3 position;
@property float rotation; 
@property (strong, nonatomic) GLKTextureInfo* textureInfo;
@property BOOL wasHit; 
@end
