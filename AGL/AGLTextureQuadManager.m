//
//  AGLTextureQuadManager.m
//  AGLFramework
//
//  Created by Ardrian Hardjono on 21/12/2013.
//
//

#import "AGLTextureQuadManager.h"
#import "AGLTextureQuad.h"
#import "AGLRingLayout.h"

@interface AGLTextureQuadManager(){
    NSMutableArray* _texturedQuads;
    
}

@property (strong, nonatomic) AGLLayout* layout;

@end

@implementation AGLTextureQuadManager

#pragma mark - Initialisation
- (id)init{
    self = [super init];
    if(self){
        _texturedQuads = [NSMutableArray array];
        self.layout = [[AGLRingLayout alloc] init];
    }
    return self;
}

- (id)initWithLayout:(AGLLayout*)layout{
    self = [super init];
    if(self){
        _texturedQuads = [NSMutableArray array];
        self.layout = layout;
    }
    return self;
}

#pragma mark - OpenGL Setup and Teardown
/** Setup OpenGL */
- (void)setupGL{
    self.effect = [[GLKBaseEffect alloc] init];
    
    for(AGLTextureQuad* texturedQuad in _texturedQuads){
        [texturedQuad setupGL];
    }
}

/** Shut down OpenGL */
- (void)tearDownGL{
    for(AGLTextureQuad* texturedQuad in _texturedQuads){
        [texturedQuad tearDownGL];
    }
    [_texturedQuads removeAllObjects];
    self.effect = nil;
    
}

/** Draw all AGLTextureQuads
 @param viewMatrix The camera view matrix
 */
- (void)drawWithViewMatrix:(GLKMatrix4)viewMatrix{
    if(!self.delegate) return;
    
    for(AGLTextureQuad* texturedQuad in _texturedQuads){
        
        GLKMatrix4 modelMatirx = [texturedQuad transformationMatrix];
        GLKMatrix4 modelViewMatrix = GLKMatrix4Multiply(viewMatrix,modelMatirx);
        self.effect.transform.modelviewMatrix = modelViewMatrix;
        if(texturedQuad.textureInfo.name){
            self.effect.texture2d0.name = texturedQuad.textureInfo.name;
            self.effect.texture2d0.enabled = true;
        } else {
            self.effect.texture2d0.enabled = false;
        }
        [self.effect prepareToDraw];
        [texturedQuad draw];
    }
    
    
}

/** Called each frame to update objects if necessary */
- (void)update{
    if(!self.delegate) return;
    
    // TODO: This class should take in some time values
    
}

/** Call this method when a new layout neds to occur. An example of this would
    be if the number of objects in the data source changed.
 */
- (void)updateLayout{
    // Destroy all current objects
    for(AGLTextureQuad* texturedQuad in _texturedQuads){
        [texturedQuad tearDownGL];
    }
    [_texturedQuads removeAllObjects];
    
    NSInteger numItems = [self.delegate numItemsInQuadSet];
    
    for(NSUInteger i=0;i<numItems;i++){
        AGLTextureQuad* textureQuad = [[AGLTextureQuad alloc] init];
        [textureQuad setupGL];
        [_texturedQuads addObject:textureQuad];
        
        textureQuad.name = [NSString stringWithFormat:@"Object: %d",(i+1)];
        textureQuad.position = [self.layout positionForObjectAtIndex:i];
        textureQuad.rotation = [self.layout rotationForObjectAtIndex:i];
        
        NSURL* url = [self.delegate urlForItemInQuadSetWithIndex:i];
        [textureQuad setTextureWithURL:url withShareGroup:self.context.sharegroup];
    }

}

#pragma mark - Public Interface
- (BOOL)didHitObject:(CGPoint)location
      withViewMatrix:(GLKMatrix4)viewMatrix
    projectionMatrix:(GLKMatrix4)projectionMatrix
            viewport:(int*)viewport
            hitIndex:(NSUInteger*)hitIndex{
    bool result = false;
    
    int i = 0;
    AGLTextureQuad* hitQuad = nil;
    float hit_z = -1000.0f;
    int currentHitIndex = 0;
    
    for(AGLTextureQuad* texturedQuad in _texturedQuads){
        GLKMatrix4 modelMatirx = [texturedQuad transformationMatrix];
        GLKMatrix4 modelViewMatrix = GLKMatrix4Multiply(viewMatrix,modelMatirx);
        GLKVector3 window_coord = GLKVector3Make(location.x,location.y, 0.0f);
        GLKVector3 near_pt = GLKMathUnproject(window_coord,
                                              modelViewMatrix,
                                              projectionMatrix,
                                              &viewport[0],
                                              &result);
        
        window_coord = GLKVector3Make(location.x,location.y, 1.0f);
        GLKVector3 far_pt = GLKMathUnproject(window_coord,
                                             modelViewMatrix,
                                             projectionMatrix,
                                             &viewport[0],
                                             &result);
        
        float z_magnitude = fabs(far_pt.z-near_pt.z);
        float near_pt_factor = fabs(near_pt.z)/z_magnitude;
        float far_pt_factor = fabs(far_pt.z)/z_magnitude;
        GLKVector3 final_pt = GLKVector3Add( GLKVector3MultiplyScalar(near_pt, far_pt_factor), GLKVector3MultiplyScalar(far_pt, near_pt_factor));
        
        //NSLog(@"Final Pt: %@",NSStringFromGLKVector3(final_pt));
        
        // Transform the final point from model space back into world space
        GLKVector4 final_pt_4 = GLKVector4Make(final_pt.x,final_pt.y,final_pt.z,1.0f);
        final_pt_4 = GLKMatrix4MultiplyVector4(modelMatirx, final_pt_4);
        
        
        if([texturedQuad isPointOnPlane:final_pt]){
            if(!hitQuad){
                hitQuad = texturedQuad;
                hit_z = final_pt_4.z;
                currentHitIndex = i;
            } else {
                if(final_pt_4.z > hit_z){
                    hitQuad = texturedQuad;
                    hit_z = final_pt_4.z;
                    currentHitIndex = i;
                }
            }
            
            
            NSLog(@"Touch landed on quad: %d",i);
            NSLog(@"Final Pt: %@ World Final: %@",NSStringFromGLKVector3(final_pt),
                  NSStringFromGLKVector4(final_pt_4));
        }
        
        i++;
        [texturedQuad showHit:NO];
    }
    
    if(hitQuad){
        NSLog(@"Quad hit at z=%f",hit_z);
        [hitQuad showHit:YES];
        *hitIndex = (NSUInteger)currentHitIndex;
        return YES;
    }
    return NO;
}

@end
