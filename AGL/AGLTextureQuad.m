//
//  AGLTextureQuad.m
//  AGLFramework
//
//  Created by Ardrian Hardjono on 21/12/2013.
//
//

#import "AGLTextureQuad.h"
#import "AGLVertex.h"

// Single Quad Face.
// (Vertex), (Color), (Tex Coord)
//const AGLVertex QuadVertices[] = {
//    {{1, -1, 1}, {1, 0, 0, 1}, {1, 0}},
//    {{1, 1, 1}, {0, 1, 0, 1}, {1, 1}},
//    {{-1, 1, 1}, {0, 0, 1, 1}, {0, 1}},
//    {{-1, -1, 1}, {0, 0, 0, 1}, {0, 0}},
//};

const GLubyte QuadIndices[] = {
    // Front
    0, 1, 2,
    2, 3, 0
};

@interface AGLTextureQuad (){
    GLuint _quadVertexBuffer;
    GLuint _quadIndexBuffer;
    GLuint _quadVertexArray;
    
    AGLVertex *_vertexBuffer;
}

@property (strong,nonatomic) GLKTextureLoader* textureLoader;

@end

@implementation AGLTextureQuad

- (id)init{
    self = [super init];
    if(self){
        // Create Quad Vertex List
        _vertexBuffer = NULL;
        self.width = 1.0f;
        self.height = 1.0f;
        self.position = GLKVector3Make(0, 0, 0);
        self.rotation = 0.0f;
        self.wasHit = NO;
    }
    return self;
}

- (void)setupGL{
    
    [self updateVerticies];
    
    if(_vertexBuffer){
        // Create Vertex Array
        glGenVertexArraysOES(1, &_quadVertexArray);
        glBindVertexArrayOES(_quadVertexArray);
        
        // Old stuff
        glGenBuffers(1, &_quadVertexBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, _quadVertexBuffer);
        glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(_vertexBuffer[0]), _vertexBuffer, GL_DYNAMIC_DRAW);
        
        glGenBuffers(1, &_quadIndexBuffer);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _quadIndexBuffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(QuadIndices), QuadIndices, GL_STATIC_DRAW);
        
        
        glEnableVertexAttribArray(GLKVertexAttribPosition);
        // Position Array
        glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE,
                              sizeof(AGLVertex),
                              (const GLvoid *) offsetof(AGLVertex, Position));
        // Color Array
        glEnableVertexAttribArray(GLKVertexAttribColor);
        glVertexAttribPointer(GLKVertexAttribColor, 4, GL_FLOAT, GL_FALSE,
                              sizeof(AGLVertex),
                              (const GLvoid *) offsetof(AGLVertex, Color));
        glEnableVertexAttribArray(GLKVertexAttribTexCoord0);
        // Texture Coordinate Array
        glVertexAttribPointer(GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE,
                              sizeof(AGLVertex),
                              (const GLvoid *) offsetof(AGLVertex, TexCoord));
        
        // Unbind Vertex Array
        glBindVertexArrayOES(0);
    }
}

- (void)tearDownGL{
    glDeleteBuffers(1, &_quadVertexBuffer);
    glDeleteBuffers(1, &_quadIndexBuffer);
    glDeleteVertexArraysOES(1, &_quadVertexArray);
}

- (void)setPNGWithName:(NSString*)name withShareGroup:(EAGLSharegroup*)shareGroup{
    NSDictionary * options = [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSNumber numberWithBool:YES],
                              GLKTextureLoaderOriginBottomLeft,
                              nil];
    
//    NSError * error;
    //NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:@"png"];
//    GLKTextureInfo * info = [GLKTextureLoader textureWithContentsOfFile:path options:options error:&error];
//    
//    [GLKTextureLoader textureWithContentsOfFile]
//    
//    if (info == nil) {
//        NSLog(@"Error loading file: %@", [error localizedDescription]);
//    } else {
//        self.textureInfo = info;
//    }

    // Load Texture Asynchronously
    self.textureLoader = [[GLKTextureLoader alloc] initWithSharegroup:shareGroup];
//    [self.textureLoader textureWithContentsOfFile:path
//                                          options:options 
//                                            queue:NULL
//                                completionHandler:^(GLKTextureInfo *textureInfo, NSError *outError) {
//
//                                    if (textureInfo == nil) {
//                                        NSLog(@"Error loading file: %@", [outError localizedDescription]);
//                                    } else {
//                                        NSLog(@"Finished loading texture. Width: %u",textureInfo.height);
//                                        self.textureInfo = textureInfo;
//                                    }
//    }];
    NSURL* url = [NSURL URLWithString:@"http://ppcdn.500px.org/55433328/4c4daf2f40bd6825aca78b4a4fc050fa07d1befb/3.jpg"];
    [self.textureLoader textureWithContentsOfURL:url
                                          options:options
                                            queue:NULL
                                completionHandler:^(GLKTextureInfo *textureInfo, NSError *outError) {
                                    
                                    if (textureInfo == nil) {
                                        NSLog(@"Error loading file: %@", [outError localizedDescription]);
                                    } else {
                                        NSLog(@"Finished loading texture. Width: %u",textureInfo.height);
                                        self.textureInfo = textureInfo;
                                    }
                                }];
}

- (void)setTextureWithURL:(NSURL*)url withShareGroup:(EAGLSharegroup*)shareGroup{
    NSDictionary * options = [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSNumber numberWithBool:YES],
                              GLKTextureLoaderOriginBottomLeft,
                              nil];

    // Load Texture Asynchronously
    self.textureLoader = [[GLKTextureLoader alloc] initWithSharegroup:shareGroup];

    [self.textureLoader textureWithContentsOfURL:url
                                         options:options
                                           queue:NULL
                               completionHandler:^(GLKTextureInfo *textureInfo, NSError *outError) {
                                   
                                   if (textureInfo == nil) {
                                       NSLog(@"Error loading file: %@", [outError localizedDescription]);
                                   } else {
                                       NSLog(@"Finished loading texture. Width: %u",textureInfo.height);
                                       self.textureInfo = textureInfo;
                                   }
                               }];
}

- (void)draw{
    if(self.wasHit){
        //NSLog(@"Item %@ WAS HIT",self.name);
        self.height = 2.0f;
    } else {
        self.height = 1.0f;
    }
    [self updateVerticies];
    
    if(_vertexBuffer){
        glBindVertexArrayOES(_quadVertexArray);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, 0);
        glBindVertexArrayOES(0);
    }
}

- (void)updateVerticies{
    //GLfloat vertices[9 * 4];
    if(!_vertexBuffer){
        // Create the vertex buffer
        _vertexBuffer = (AGLVertex*)malloc(4 * sizeof(AGLVertex));
        if(_vertexBuffer){
            // Could not allocate memory for this vertex array
        }
        
        // Vertex 1
        _vertexBuffer[0].TexCoord[0] = 1.0f;
        _vertexBuffer[0].TexCoord[1] = 0.0f;
        
        _vertexBuffer[0].Color[0] = 1.0f;
        _vertexBuffer[0].Color[1] = 1.0f;
        _vertexBuffer[0].Color[2] = 1.0f;
        _vertexBuffer[0].Color[3] = 1.0f;
        
        // Vertex 2
        _vertexBuffer[1].TexCoord[0] = 1.0f;
        _vertexBuffer[1].TexCoord[1] = 1.0f;
        
        _vertexBuffer[1].Color[0] = 1.0f;
        _vertexBuffer[1].Color[1] = 0.0f;
        _vertexBuffer[1].Color[2] = 1.0f;
        _vertexBuffer[1].Color[3] = 1.0f;
        
        // Vertex 3
        _vertexBuffer[2].TexCoord[0] = 0.0f;
        _vertexBuffer[2].TexCoord[1] = 1.0f;
        
        _vertexBuffer[2].Color[0] = 0.0f;
        _vertexBuffer[2].Color[1] = 1.0f;
        _vertexBuffer[2].Color[2] = 1.0f;
        _vertexBuffer[2].Color[3] = 1.0f;
        
        // Vertex 4
        _vertexBuffer[3].TexCoord[0] = 0.0f;
        _vertexBuffer[3].TexCoord[1] = 0.0f;
        
        _vertexBuffer[3].Color[0] = 1.0f;
        _vertexBuffer[3].Color[1] = 1.0f;
        _vertexBuffer[3].Color[2] = 0.0f;
        _vertexBuffer[3].Color[3] = 1.0f;
    
    }
    
    float halfWidth = self.width/2.0f;
    float halfHeight = self.height/2.0f;
    
    // Vertex 1
    _vertexBuffer[0].Position[0] = halfWidth;
    _vertexBuffer[0].Position[1] = -halfHeight;
    _vertexBuffer[0].Position[2] = 0.0f;
    
    // Vertex 2
    _vertexBuffer[1].Position[0] = halfWidth;
    _vertexBuffer[1].Position[1] = halfHeight;
    _vertexBuffer[1].Position[2] = 0.0f;
    
    // Vertex 3
    _vertexBuffer[2].Position[0] = -halfWidth;
    _vertexBuffer[2].Position[1] = halfHeight;
    _vertexBuffer[2].Position[2] = 0.0f;
    
    // Vertex 4
    _vertexBuffer[3].Position[0] = -halfWidth;
    _vertexBuffer[3].Position[1] = -halfHeight;
    _vertexBuffer[3].Position[2] = 0.0f;
    
    // Bind Buffer again with the updated values in this array
    if(_quadVertexBuffer > 0){
        glBindBuffer(GL_ARRAY_BUFFER, _quadVertexBuffer);
        glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(_vertexBuffer[0]), _vertexBuffer, GL_DYNAMIC_DRAW);
    }
   
}

- (GLKMatrix4)transformationMatrix{
    GLKMatrix4 rotationMatrix = GLKMatrix4MakeYRotation(self.rotation);
    GLKMatrix4 translationMatrix = GLKMatrix4MakeTranslation(self.position.x, self.position.y, self.position.z);
    GLKMatrix4 modelMatrix = GLKMatrix4Multiply(translationMatrix,rotationMatrix);
    return modelMatrix;
}

- (BOOL)isPointOnPlane:(GLKVector3)point{
    if(fabs(point.x) > self.width/2.0f){
        return NO;
    }
    if(fabs(point.y) > self.height/2.0f){
        return NO;
    }
    return YES;
}

- (void)showHit:(BOOL)hit{
    self.wasHit = hit;
    if(hit){
    NSLog(@"ShowHit on object name: %@",self.name);
    }
    
}

@end
