//
//  AGLRingLayout.h
//  AGLFramework
//
//  Created by Ardrian Hardjono on 21/12/2013.
//
//

#import "AGLLayout.h"

@interface AGLRingLayout : AGLLayout

@property NSInteger ringSize;
@property NSInteger ringHeight;

/** Set any time based attributes */
- (void)update;

@end
