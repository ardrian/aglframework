//
//  AGLLayout.h
//  AGLFramework
//
//  Created by Ardrian Hardjono on 21/12/2013.
//
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>

@interface AGLLayout : NSObject
@property NSInteger maxItems;

- (void)prepareLayout;
- (float)rotationForObjectAtIndex:(NSUInteger)index;
- (GLKVector3)positionForObjectAtIndex:(NSUInteger)index;

@end
