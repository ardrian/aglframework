//
//  AGLViewController.h
//  AGLFramework
//
//  Created by Ardrian Hardjono on 23/12/2013.
//
//

#import <GLKit/GLKit.h>
#import "AGLQuadSetDelegate.h"

/** This class should be subclassed for a view controller 
    which will contain the AGL Texture Quads
 */
@interface AGLViewController : GLKViewController

@property (weak,nonatomic) id<AGLQuadSetDelegate> quadSetDelegate;

@end
