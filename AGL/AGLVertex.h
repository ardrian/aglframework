//
//  AGLVertex.h
//  AGLFramework
//
//  Created by Ardrian Hardjono on 21/12/2013.
//
//

#ifndef AGLFramework_AGLVertex_h
#define AGLFramework_AGLVertex_h

typedef struct {
    GLfloat Position[3];
    GLfloat Color[4];
    GLfloat TexCoord[2];
} AGLVertex;

#endif
