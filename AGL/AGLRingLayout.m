//
//  AGLRingLayout.m
//  AGLFramework
//
//  Created by Ardrian Hardjono on 21/12/2013.
//
//

#import "AGLRingLayout.h"

@interface AGLRingLayout(){
        
}

@end

@implementation AGLRingLayout

- (void)prepareLayout{
    
}

- (GLKVector3)positionForObjectAtIndex:(NSUInteger)index{
    return GLKVector3Make((float)index * 0.5f,(float)index * 1.f,0.0f);
}

- (float)rotationForObjectAtIndex:(NSUInteger)index{
    return 0.0f;
}

- (void)update{
    
}


@end
