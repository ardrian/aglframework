//
//  AGLLayout.m
//  AGLFramework
//
//  Created by Ardrian Hardjono on 21/12/2013.
//
//

#import "AGLLayout.h"

@implementation AGLLayout

- (void)prepareLayout{

}

- (GLKVector3)positionForObjectAtIndex:(NSUInteger)index{
    return GLKVector3Make(0,0,0);
}

- (float)rotationForObjectAtIndex:(NSUInteger)index{
    return 0.0f;
}

@end
